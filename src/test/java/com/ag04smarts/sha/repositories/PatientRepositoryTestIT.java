package com.ag04smarts.sha.repositories;

import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.domain.Symptom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PatientRepositoryTestIT {

    @Autowired
    private PatientRepository patientRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findDistinctByPatientMedicalRecordSymptomsDescriptionIn() {
        List<String> list = Arrays.asList("Coughing", "Fever");
        List<Patient> patients = patientRepository.findDistinctByPatientMedicalRecordSymptomsDescriptionIn(list);
        Set<Symptom> symptoms= new HashSet<Symptom>();
        symptoms.add(new Symptom("Coughing"));

        assertEquals(symptoms, patients.iterator().next().getPatientMedicalRecord().getSymptoms());

    }
}