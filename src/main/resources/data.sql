INSERT INTO patients (first_name, last_name, email, age, phone_number, gender, enlistment_date, status) VALUES
  ('Teodoto', 'Calderone', 'teodotocalderone@gmail.com', 26, '+39-385-555-358', 'MALE', parsedatetime('17-07-2020 18:47', 'dd-MM-yyyy hh:mm'), 'DIAGNOSED'),
  ('Ezio', 'Mestre', 'eziomestre@gmail.com', 35, '+39-320-555-083', 'MALE', parsedatetime('01-03-2019 11:24', 'dd-MM-yyyy hh:mm'), 'CURED'),
  ('Paola', 'Nardozzi', 'paolanardozzi@gmail.com', 44, '+39-370-555-801', 'FEMALE', parsedatetime('22-05-2020 14:01', 'dd-MM-yyyy hh:mm'), 'UNDER_TREATMENT');

INSERT INTO patient_medical_records (id, diagnosis, treatment, patient_id) VALUES
  (1,'Deviated septum', 'Nose surgery',1),
  (2,'COVID-19 positive', 'Self isolation',2),
  (3,'Arm fracture', 'Arm cast',3);

INSERT INTO doctors (first_name, last_name, doctor_expertise) VALUES
  ('Augusto', 'Varallo', 'PLASTIC_SURGEON'),
  ('Leone', 'Scalzi', 'GENERAL_DOCTOR'),
  ('Raffaella', 'Condo', 'SPORTS_MEDICINE_SPECIALIST');

INSERT INTO appointments (appointment_date, patient_id, doctor_id) VALUES
  (parsedatetime('23-07-2020 13:00', 'dd-MM-yyyy hh:mm'),1,3),
  (parsedatetime('17-08-2020 14:30', 'dd-MM-yyyy hh:mm'),2,2),
  (parsedatetime('01-09-2020 11:00', 'dd-MM-yyyy hh:mm'),3,1);

INSERT INTO treatment_histories (patient_id, doctor_id, treatment_remark, old_status, new_status) VALUES
  (2, 3, 'Broken bone healed correctly, physical therapy recommended', 'UNDER_TREATMENT', 'CURED' ),
  (1, 2, 'COVID-19 diagnosis, patient says he got a virus from a friend that traveled', 'UNDER_DIAGNOSIS', 'DIAGNOSED'),
  (3, 1, 'Wide deviation of septum, quickly transfered to plastic surgery department', 'DIAGNOSED', 'UNDER_TREATMENT');

INSERT INTO symptoms (description) VALUES
  ('Sharp pain on radial side of right arm'),
  ('Coughing'),
  ('Inability to breath through right nostril');

INSERT INTO record_symptoms (symptom_id, patient_medical_record_id) VALUES
  (3,1),
  (2,2),
  (1,3);



