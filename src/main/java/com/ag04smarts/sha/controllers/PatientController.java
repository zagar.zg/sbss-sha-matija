package com.ag04smarts.sha.controllers;

import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.exceptions.ErrorMessage;
import com.ag04smarts.sha.exceptions.PatientNotFoundException;
import com.ag04smarts.sha.services.PatientService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@RestController
@RequestMapping("/api/patient")
public class PatientController {

    private final PatientService patientService;

    public PatientController(PatientService patientService) {

        this.patientService = patientService;
    }

    @GetMapping
    public ResponseEntity<Iterable<Patient>> getPatients() {

        return ResponseEntity.ok(patientService.getAllPatients());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Patient> getPatientById(@PathVariable Long id) {

        return ResponseEntity.ok(patientService.getPatientById(id));
    }

    @GetMapping("/filter/ageAndDate")
    public ResponseEntity<Iterable<Patient>> getPatientsOlderThanAgeAndEnlistedAfterDate() throws ParseException {
        int age = 21;
        Date date = new SimpleDateFormat("dd.MM.yyyy").parse("01.01.2020");
        return ResponseEntity.ok(patientService.getAllPatientsOlderThanAgeAndEnlistedAfterDate(age, date));
    }

    @GetMapping("/filter/symptoms")
    public ResponseEntity<Iterable<Patient>> getAllPatientsWithSymptoms() {
        return ResponseEntity.ok(patientService.getAllPatientsWithSymptoms(Arrays.asList("Coughing", "Fever")));
    }

    @PostMapping
    public ResponseEntity<Patient> addPatient(@RequestBody Patient patient) {

        return ResponseEntity.ok(patientService.addPatient(patient));

    }

    @PutMapping("/{id}")
    public ResponseEntity<Patient> updatePatient(@RequestBody Patient newPatient, @PathVariable Long id) {

        return ResponseEntity.ok(patientService.updatePatient(newPatient, id));

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePatientById(@PathVariable Long id) {

        patientService.deletePatientById(id);
        return new ResponseEntity<>("Patient with ID  = " + id + " successfully deleted!", HttpStatus.OK);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorMessage> handleException(PatientNotFoundException exc) {

        ErrorMessage error = new ErrorMessage();

        error.setStatus(HttpStatus.NOT_FOUND.value());
        error.setMessage(exc.getMessage());
        error.setTimeStamp(System.currentTimeMillis());

        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorMessage> handleException(Exception exc) {

        ErrorMessage error = new ErrorMessage();

        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setMessage(exc.getMessage());
        error.setTimeStamp(System.currentTimeMillis());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
