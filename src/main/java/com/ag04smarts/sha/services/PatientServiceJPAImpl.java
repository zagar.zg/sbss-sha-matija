package com.ag04smarts.sha.services;

import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.exceptions.PatientNotFoundException;
import com.ag04smarts.sha.repositories.PatientRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.*;

@Profile({"dev", "default"})
@Service
public class PatientServiceJPAImpl implements PatientService{

    private final PatientRepository patientRepository;

    public PatientServiceJPAImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public Iterable <Patient> getAllPatients() {

        return patientRepository.findAll();
    }

    @Override
    public Patient getPatientById(Long id) {
        return patientRepository.findById(id).orElseThrow(() -> new PatientNotFoundException("Patient id not found ID: " + id));
    }

    @Override
    public Patient addPatient(Patient patient)  {
        return patientRepository.save(patient);
    }

    @Override
    public Patient updatePatient(Patient newPatient, Long id) {
        patientRepository.findById(id).orElseThrow(() -> new PatientNotFoundException("Patient id not found ID: " + id));
        newPatient.setId(id);
        return patientRepository.save(newPatient);
    }

    @Override
    public ResponseEntity deletePatientById(Long id) {
        patientRepository.findById(id).orElseThrow(() -> new PatientNotFoundException("Patient id not found ID: " + id));
        patientRepository.deleteById(id);
        return ResponseEntity.ok("Successfully deleted");
    }


    @Override
    public Iterable<Patient> getAllPatientsOlderThanAgeAndEnlistedAfterDate(int age, Date enlistedDate) {
        return  patientRepository.findDistinctByAgeGreaterThanAndEnlistmentDateAfter(age, enlistedDate);
    }

    @Override
    public Iterable<Patient> getAllPatientsWithSymptoms(List<String> symptoms) {
        return patientRepository.findDistinctByPatientMedicalRecordSymptomsDescriptionIn(symptoms);
    }

}
