package com.ag04smarts.sha.services;

import com.ag04smarts.sha.domain.Patient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
public interface PatientService {

    Iterable <Patient> getAllPatients();
    Patient getPatientById(Long id);
    Patient updatePatient(Patient newPatient, Long id);
    Patient addPatient(Patient patient);
    ResponseEntity deletePatientById(Long id);
    Iterable<Patient> getAllPatientsOlderThanAgeAndEnlistedAfterDate(int age, Date enlistedDate);
    Iterable<Patient> getAllPatientsWithSymptoms(List<String> symptoms);
}
