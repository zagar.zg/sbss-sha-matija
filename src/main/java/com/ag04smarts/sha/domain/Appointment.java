package com.ag04smarts.sha.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Data
@Table(name = "appointments")
public class Appointment extends BaseEntity{

    @ManyToOne
    private Patient patient;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "appointment_date")
    private Date appointmentDate;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private Doctor doctor;

}
