package com.ag04smarts.sha.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.*;

@Entity
@Data
@Table(name = "treatment_histories")
public class PatientTreatmentHistory extends BaseEntity{

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "patient_id")
    @JsonIgnore
    private Patient patient;

    @ManyToOne
    @JsonIgnore
    private Doctor doctor;

    @Column(name = "treatment_remark")
    private String treatmentRemark;

    @Enumerated(value = EnumType.STRING)
    private Status oldStatus;

    @Enumerated(value = EnumType.STRING)
    private Status newStatus;

}
