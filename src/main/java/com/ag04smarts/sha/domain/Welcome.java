package com.ag04smarts.sha.domain;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@PropertySources({
        @PropertySource("classpath:application.properties"),
        @PropertySource("classpath:sha.properties")
})
public class Welcome implements InitializingBean {

    @Value("${clinic.web.url}")
    private String websiteLink;

    @Value("${clinic.url.linkedin}")
    private String clinicLinkedin;

    @Value("${clinic.url.twitter}")
    private String clinicTwitter;

    private final String clinicAddress;

    private final String clinicTelephone;

    public Welcome(@Value("${clinic.address}")String clinicAddress, @Value("${clinic.telephone}")String clinicTelephone) {
        this.clinicAddress = clinicAddress;
        this.clinicTelephone = clinicTelephone;
    }

    @Value("${clinic.name}")
    public void welcomeMsg(String clinicName) {
        System.out.println("Welcome to " + clinicName);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("You can look us up on this website " + websiteLink);
    }

    public void showLinkedin() {
        System.out.println("You can look us up on Linkedin " + clinicLinkedin);
    }

    public void showTwitter() {
        System.out.println("You can look us up on Twitter also " + clinicTwitter);
    }

    public void showAddressAndTel() {
        System.out.println("Our address is " + clinicAddress + " and our telephone is " + clinicTelephone);
        log.info("Loaded all files...");
    }
}
