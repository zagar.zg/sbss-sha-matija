package com.ag04smarts.sha.domain;

import javax.persistence.Table;

@Table(name = "status")
public enum Status {
    ENLISTED, UNDER_DIAGNOSIS, DIAGNOSED,
    UNDER_TREATMENT, CURED
}
