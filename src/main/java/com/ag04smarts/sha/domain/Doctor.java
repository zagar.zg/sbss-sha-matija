package com.ag04smarts.sha.domain;

import lombok.Data;
import javax.persistence.*;

@Entity
@Data
@Table(name = "doctors")
public class Doctor extends Person{

    @Enumerated(value = EnumType.STRING)
    private DoctorExpertise doctorExpertise;


}
