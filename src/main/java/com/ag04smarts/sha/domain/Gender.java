package com.ag04smarts.sha.domain;

import javax.persistence.Table;

@Table(name = "gender")
public enum Gender {
    MALE, FEMALE
}
