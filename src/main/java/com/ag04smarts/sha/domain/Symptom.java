package com.ag04smarts.sha.domain;

import lombok.Data;
import javax.persistence.*;

@Entity
@Data
@Table(name = "symptoms")
public class Symptom extends BaseEntity{

    @Column(name = "description")
    private String description;

    public Symptom(String description) {
        this.description = description;
    }

    public Symptom() {};
}
