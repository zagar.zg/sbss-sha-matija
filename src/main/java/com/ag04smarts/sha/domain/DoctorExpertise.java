package com.ag04smarts.sha.domain;

import javax.persistence.Table;


@Table(name = "doctor_expertise")
public enum DoctorExpertise {
    PEDIATRICIAN, SPORTS_MEDICINE_SPECIALIST,
    PLASTIC_SURGEON, GENERAL_DOCTOR
}
