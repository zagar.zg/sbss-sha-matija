package com.ag04smarts.sha.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Table(name = "patient_medical_records")
public class PatientMedicalRecord extends BaseEntity{


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    @JsonIgnore
    private Patient patient;

    @Column(name = "diagnosis")
    private String diagnosis;

    @Column(name = "treatment")
    private String treatment;

    @ManyToMany
    @JoinTable(name="record_symptoms",
            joinColumns = @JoinColumn(name = "patient_medical_record_id"), inverseJoinColumns = @JoinColumn(name = "symptom_id"))
    private Set<Symptom> symptoms;
}
