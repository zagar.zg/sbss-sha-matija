package com.ag04smarts.sha.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Set;

@Entity
@Data
@Table(name = "patients")
public class Patient extends Person{

    @Column(name = "email")
    private String email;

    @Column(name = "age")
    private int age;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "enlistment_date")
    private Date enlistmentDate;

    @Enumerated(value = EnumType.STRING)
    private Status status;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "patient")
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private Set<PatientTreatmentHistory> treatmentHistories;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "patient")
    @EqualsAndHashCode.Exclude
    private PatientMedicalRecord patientMedicalRecord;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createdDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updatedDate;

    @PrePersist
    private void onPersist() {
        createdDate = new Date();
    }

    @PreUpdate
    private void onUpdate() {
        updatedDate = new Date();
    }

}
