package com.ag04smarts.sha.repositories;

import com.ag04smarts.sha.domain.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;


@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {
    List<Patient> findDistinctByAgeGreaterThanAndEnlistmentDateAfter(int age, Date enlistedDate);
    List<Patient> findDistinctByPatientMedicalRecordSymptomsDescriptionIn(List<String> symptoms);

}
