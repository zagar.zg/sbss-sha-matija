package com.ag04smarts.sha.repositories;

import com.ag04smarts.sha.domain.Symptom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SymptomRepository extends CrudRepository<Symptom, Long> {
}
