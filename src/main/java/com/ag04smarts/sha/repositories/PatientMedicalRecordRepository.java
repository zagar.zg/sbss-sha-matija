package com.ag04smarts.sha.repositories;

import com.ag04smarts.sha.domain.PatientMedicalRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientMedicalRecordRepository extends CrudRepository<PatientMedicalRecord, Long> {
}
