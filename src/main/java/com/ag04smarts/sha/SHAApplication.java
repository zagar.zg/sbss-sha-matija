package com.ag04smarts.sha;

import com.ag04smarts.sha.domain.Patient;
import com.ag04smarts.sha.domain.Welcome;
import com.ag04smarts.sha.repositories.PatientRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.Optional;


@SpringBootApplication
public class SHAApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SHAApplication.class, args);
		Welcome initializer = (Welcome)ctx.getBean("welcome");


		initializer.showLinkedin();
		initializer.showTwitter();
		initializer.showAddressAndTel();

		PatientRepository repository = ctx.getBean(PatientRepository.class);
		Patient newPatient = new Patient();
		newPatient.setFirstName("Ivan");
		newPatient.setLastName("Lokonic");
		repository.save(newPatient);

		Optional<Patient> foundPatient = repository.findById(2l);
		Patient patientToUpdate = foundPatient.get();
		patientToUpdate.setFirstName("Evan");
		patientToUpdate.setLastName("Larsson");
		repository.save(patientToUpdate);

	}


}
